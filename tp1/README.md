# TP1 : Maîtrise réseau du poste

## I. Basics

### ️ Carte réseau WiFi

```
C:\Users\Cacahuète>ipconfig /all

[...]
Carte réseau sans fil Wi-Fi:
   Adresse physique . . . . . . . . . . . : 84-5C-F3-F5-18-F6
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.36.214(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . : 192.168.36.178
   Serveur DHCP . . . . . . . . . . . . . : 192.168.36.178
[...]
```

+ Adresse MAC : `84-5C-F3-F5-18-F6`
+ Adresse IP : `192.168.36.214`
+ Masque de sous-réseau CIDR : `/24`
+ Masque de sous-réseau décimal : `255.255.255.0`

### Déso pas déso

+ Adresse réseau LAN : `192.168.36.0`
+ Adresse broadcast : `192.168.36.255`
+ Nombre d'adresses IP : `253`

### Hostname

```
C:\Users\Cacahuète>hostname
Newmoon
```

### Passerelle du réseau

```
Passerelle par défaut. . . . . . . . . : 192.168.36.178
```

```
C:\Users\Cacahuète>arp -a
[...]
Interface : 192.168.36.214 --- 0x5
  Adresse Internet      Adresse physique      Type
  192.168.36.178        de-c7-aa-9f-a6-53     dynamique
```

+ Adresse IP passerelle : `192.168.36.178`
+ Adresse MAC passerelle : `de-c7-aa-9f-a6-53`

### Serveur DHCP et DNS

```
Serveur DHCP . . . . . . . . . . . . . : 192.168.36.178

Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                         1.1.1.1
```

+ Serveur DHCP: `192.168.36.178`
+ Serveur DNS: `8.8.8.8`

### Table de routage

La route par défaut est :

```
C:\Users\Cacahuète>route print
[...]
Destination réseau    Masque réseau  Adr. passerelle   Adr. interface Métrique
          0.0.0.0          0.0.0.0     10.33.79.254     10.33.76.206     30
[...]
```

## II. Go further

### Hosts ?

```
C:\Windows\System32>nano C:\Windows\System32\drivers\etc\hosts

+ 1.1.1.1      b2.hello.vous
```

Je peux ping le domaine :
```
C:\Windows\System32>ping b2.hello.vous

Envoi d’une requête 'ping' sur b2.hello.vous [1.1.1.1] avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=12 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=11 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=11 ms TTL=57
Réponse de 1.1.1.1 : octets=32 temps=10 ms TTL=57

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 10ms, Maximum = 12ms, Moyenne = 11ms
```

### Go mater une vidéo youtube et déterminer, pendant qu'elle tourne

Voici [le scan Wireshark](pcaps/youtube.pcapng).

+ L'adresse IP du serveur auquel vous êtes connectés pour regarder la vidéo : `91.68.245.16`
+ Le port du serveur auquel vous êtes connectés : `443 (https)`
+ Le port que votre PC a ouvert en local pour se connecter au port du serveur distant : `49993`

### Requêtes DNS

À quelle adresse IP correspond le nom de domaine `www.ynov.com` : 

```
C:\Windows\System32>ping www.ynov.com

Envoi d’une requête 'ping' sur www.ynov.com [172.67.74.226] avec 32 octets de données :
Réponse de 172.67.74.226 : octets=32 temps=11 ms TTL=57
```

Le nom de domaine correspond à l'adresse IP : `172.67.74.226`

```
C:\Windows\System32>nslookup 174.43.238.89
Serveur :   dns.google
Address:  8.8.8.8

Nom :    89.sub-174-43-238.myvzw.com
Address:  174.43.238.89
```

L'IP `174.43.238.89` correspond au nom de domaine : `89.sub-174-43-238.myvzw.com`

### Hop hop hop

Par combien de machines vos paquets passent quand vous essayez de joindre www.ynov.com :

```
C:\Windows\System32>tracert www.ynov.com

Détermination de l’itinéraire vers www.ynov.com [104.26.11.233]
avec un maximum de 30 sauts :

  1     3 ms     1 ms     1 ms  10.33.79.254
  2     4 ms     2 ms     2 ms  145.117.7.195.rev.sfr.net [195.7.117.145]
  3     3 ms     2 ms     2 ms  237.195.79.86.rev.sfr.net [86.79.195.237]
  4     9 ms     6 ms     4 ms  196.224.65.86.rev.sfr.net [86.65.224.196]
  5    11 ms    11 ms    11 ms  12.148.6.194.rev.sfr.net [194.6.148.12]
  6    12 ms    10 ms    12 ms  12.148.6.194.rev.sfr.net [194.6.148.12]
  7    13 ms    10 ms    10 ms  141.101.67.48
  8    11 ms    12 ms    11 ms  172.71.124.4
  9    18 ms    10 ms    10 ms  104.26.11.233

Itinéraire déterminé.
```

On passe par 9 machines pour joindre le nom de domaine `www.ynov.com`

### IP publique

```
C:\Windows\System32>nslookup myip.opendns.com resolver1.opendns.com
Serveur :   dns.opendns.com
Address:  208.67.222.222

Réponse ne faisant pas autorité :
Nom :    myip.opendns.com
Address:  195.7.117.146
```

L'IP publique est `195.7.117.146`.

### Scan réseau

*C'est illégal non ?*

## III. Le requin

### Capture ARP
Capturez un échange ARP entre votre PC et la passerelle du réseau

[Voici la capture ARP](pcaps/arp.pcapng)

### Capture DNS
Capturez une requête DNS vers le domaine de votre choix (en l'occurrence `cacahuete.dev`, mon nom de domaine) et la réponse

J'ai utilisé le filtre wireshark `dns`

[Voici la capture DNS](pcaps/dns.pcapng)

### Capture TCP
Effectuez une connexion qui sollicite le protocole TCP

J'ai utilisé le filtre wireshark `tcp.port == 25565` (le port de Minecraft car c'est lui que j'ai utilisé pour cette question)

[Voici la capture TCP](pcaps/minecraft.pcapng)